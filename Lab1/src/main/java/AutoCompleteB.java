import redis.clients.jedis.Jedis;
import redis.clients.jedis.SortingParams;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;
import java.util.Set;

public class AutoCompleteB {

    public static void main(String[] args) {
        Jedis jedis = new Jedis("localhost");
        String choice;
        Scanner sc = new Scanner(System.in);

        // Read file and place them on redis
        File f = new File("src/main/java/nomes-registados-2016.csv");
        try (BufferedReader br = new BufferedReader(new FileReader(f))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] columns = line.split(",");
                String name = columns[0];
                String sex = columns[1];
                String numRecords = columns[2];
                jedis.sadd("names", name);
                jedis.hset(name, "registos", numRecords);
            }
        } catch (Exception ex) {
            System.out.printf(ex.getMessage());
        }


        do {
            System.out.print("Search for ('Enter' for quit): ");
            choice = sc.nextLine();

            getAutocomplete(jedis, choice);
        } while (!choice.equals(""));
    }

    public static void getAutocomplete(Jedis jedis, String choice) {
        jedis.sort("names", new SortingParams().by("names:*->registos asc"));
        String aux = "*" + choice + "*";
        Set<String> names = jedis.keys(aux);

        for (String s : names) {
            System.out.println(s);
        }
    }
}

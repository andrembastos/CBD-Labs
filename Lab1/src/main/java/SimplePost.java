import redis.clients.jedis.Jedis;

import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import java.util.List;

public class SimplePost {
    private Jedis jedis;
    public static String USERS = "users";

    public SimplePost() {
        this.jedis = new Jedis("localhost");
    }

    public void saveUser(String username) {
        jedis.sadd(USERS, username);
    }

    public Set<String> getUser() {
        return jedis.smembers(USERS);
    }

    public void saveUserList(String username) {
        jedis.lpush("example", username);
    }

    public List<String> getUserList() {
        return jedis.lrange("example", 0, 10);
    }

    public void saveUserMap(String username) {
        jedis.hset("user", "username", username);
    }

    public Map<String, String> getUserMap() {
        return jedis.hgetAll("user");

    }

    public static void main(String args[]) {
        SimplePost board = new SimplePost();
        String[] users = {"Ana", "Pedro", "Maria", "Luis"};
        for (String user : users) {
            board.saveUser(user);
            board.saveUserList(user);
        }

        for (String s : board.getUser()) {
            System.out.println(s);
        }

        System.out.println(board.getUserList());

        board.saveUserMap("TesteHashMap");
        System.out.println(board.getUserMap());
    }
}

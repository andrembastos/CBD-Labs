import redis.clients.jedis.Jedis;

import java.io.*;
import java.util.Scanner;
import java.util.Set;

public class AutoComplete {

    public static void main(String[] args) {
        Jedis jedis = new Jedis("localhost");
        String choice;
        Scanner sc = new Scanner(System.in);

        // Read file and place them on redis
        File f = new File("src/main/java/female-names.txt");
        try (BufferedReader br = new BufferedReader(new FileReader(f))) {
            String line;
            while ((line = br.readLine()) != null) {
                jedis.sadd(line, "");
            }
        } catch (Exception ex) {
            System.out.printf(ex.getMessage());
        }


        do {
            System.out.print("Search for ('Enter' for quit): ");
            choice = sc.nextLine();

            getAutocomplete(jedis, choice);
        } while (!choice.equals(""));
    }

    public static void getAutocomplete(Jedis jedis, String choice) {
        String aux = "*"+choice+"*";
        Set<String> names = jedis.keys(aux);

        for (String s : names) {
            System.out.println(s);
        }
    }
}

package com.company;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Indexes;
import org.bson.Document;

import java.lang.reflect.Array;
import java.util.*;

import static com.mongodb.client.model.Filters.eq;

public class Main {
    static int choice;
    static Scanner scanner = new Scanner(System.in);
    static MongoClient mongoClient = new MongoClient("localhost", 27017);
    static MongoDatabase database = mongoClient.getDatabase("cbd");
    static MongoCollection<Document> collection = database.getCollection("rest");
    public static void main(String[] args) {
        do {
            System.out.println("Enter a choice");
            System.out.println("1 - Add restaurant");
            System.out.println("2 - Remove restaurant");
            System.out.println("3 - Search restaurant");
            System.out.println("4 - countLocations");
            System.out.println("5 - countRestByLocation");
            System.out.println("6 - countRestByLocationAndGastronomy");
            System.out.println("7 - getRestWithNameCloserTo");
            System.out.println("8 - createIndexes");
            System.out.println("0 - Quit");
            choice = scanner.nextInt();
            scanner.nextLine();
            switch(choice) {
                case 1:
                    addRestaurant();
                    break;
                case 2:
                    removeRestaurant();
                    break;
                case 3:
                    searchRestaurant();
                    break;
                case 4:
                    countLocations();
                    break;
                case 5:
                    countRestByLocation();
                    break;
                case 6:
                    countRestByLocationAndGastronomy();
                    break;
                case 7:
                    getRestWithNameCloserTo();
                    break;
                case 8:
                    createIndexes();
                    break;
            }
        }while(choice != 0);
    }

    private static void createIndexes() {
        collection.createIndex(Indexes.hashed("localidade"));
        collection.createIndex(Indexes.hashed("gastronomia"));
        collection.createIndex(Indexes.text("nome"));
    }

    private static void getRestWithNameCloserTo() {
        System.out.println("Name? ");
        String name = scanner.nextLine();

        Block<Document> printBlock = new Block<Document>() {

            public void apply(final Document document) {
                System.out.println(document.toJson());
            }
        };

        collection.find(Filters.text(name)).forEach(printBlock);


    }

    private static void countRestByLocationAndGastronomy() {

    }

    private static void countRestByLocation() {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        for (Document document : collection.find()) {
            String loc = document.get("localidade").toString();
            if(map.containsKey(loc)) {
                int beforeValue = map.get(loc);
                map.put(loc, beforeValue+1);
            }else {
                map.put(loc, 1);
            }
        }
        System.out.println("Numero de restaurantes por localidade e gastronomia: ");
        for (Object o : map.entrySet()) {
            Map.Entry pair = (Map.Entry) o;
            System.out.println(pair.getKey() + " - " + pair.getValue());
        }
    }

    private static void countLocations() {
        int aux = 0;
        DistinctIterable<String> numberLocations = collection.distinct("localidade",String.class);
        for(String s : numberLocations) {
            aux++;
        }
        System.out.println("Número de localidades distintas "+aux);
    }


    static void addRestaurant() {

        System.out.println("Name?");
        String name = scanner.nextLine();
        System.out.println("Location");
        String location = scanner.nextLine();
        System.out.println("Gastronomy");
        String gastronomy = scanner.nextLine();

        Document doc = new Document();
        doc.append("nome", name);
        doc.append("localidade", location);
        doc.append("gastronomia", gastronomy);
        collection.insertOne(doc);
        System.out.println("Restaurant inserido com sucesso");
    }


    static void searchRestaurant() {
        System.out.println("Restaurant name?");
        String name = scanner.nextLine();
        Document doc = collection.find(eq("nome", name)).first();
        if(doc != null) {
            System.out.println(doc.toJson());
        }else {
            System.out.println("Restaurant does not exist");
        }

    }

    static void removeRestaurant() {
        System.out.println("Restaurant name to remove?");
        String name = scanner.nextLine();
        Document doc = collection.find(eq("nome", name)).first();
        if(doc != null) {
            collection.deleteOne(doc);
            System.out.println("Restaurant deleted");
        }else {
            System.out.println("Restaurant does not exist");
        }
    }
}
